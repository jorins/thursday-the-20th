#!/usr/bin/env sh

if [ "$FEDI_USER" = "" ]; then
  echo 'Please specify $FEDI_USER as an environment variable.'
  exit 1
fi

if [ "$FEDI_INST" = "" ]; then
  echo 'Please specify $FEDI_INST as an environment variable.'
  exit 2
fi


IS_THU=$(test "$(date +%u)" = "4"  && echo "1" || echo "0")
 IS_20=$(test "$(date +%d)" = "20" && echo "1" || echo "0")
IS_APR=$(test "$(date +%m)" = "04" && echo "1" || echo "0")
COND="${IS_THU}${IS_20}${IS_APR}"

case $COND in
  111)  # Thursday April 20th
    IMG="thu-20-apr.jpg"
    MSG="The day has come 🔥"
    ;;
  110)  # Thursday 20th
    IMG="thu-20.jpg"
    MSG="I'm back in town for tonight only!"
    ;;
  101)  # Thursday, April
    IMG="thu.jpg"
    MSG="Looks like the weekend is about to hit!"
    ;;
  100)  # Thursday
    IMG="thu.jpg"
    MSG="Little teaser of my upcoming single."
    ;;
  011)  # April 20th
    IMG="20-apr.jpg"
    MSG="I am legally obligated to discourage you from using drugs."
    ;;
  010)  # 20th
    IMG="20.jpg"
    MSG="New single out soon!"
    ;;
  001)  # April
    exit 0;
    ;;
  000)  # Nothing
    exit 0;
    ;;
esac

PATH="$PATH:$HOME/.bin"
madonctl --login "$FEDI_USER" --instance "$FEDI_INSTANCE" post --file "$(pwd)/img/$IMG" "$MSG"
